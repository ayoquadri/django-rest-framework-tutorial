from rest_framework import serializers
from .models import Snippet, LANGUAGE_CHOICES, STYLE_CHOICES
from django.contrib.auth.models import User
# Register your models here.
class SnippetSerializer(serializers.ModelSerializer):
    #The first part of the serializer class defines the fields that get serialized/deserialized
    # It's important to remember that ModelSerializer classes don't do anything particularly magical, 
    # they are simply a shortcut for creating serializer classes:
        # An automatically determined set of fields.
        # Simple default implementations for the create() and update() methods.
        
    # The untyped ReadOnlyField is always read-only, and will be used for serialized representations, 
    # but will not be used for updating model instances when they are deserialized
    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Snippet
        fields = ('id','title','code','linenos','language','style','owner')

class UserSerializer(serializers.ModelSerializer):
    # Because 'snippets' is a reverse relationship on the User model, it will not be included by 
    # default when using the ModelSerializer class, so we needed to add an explicit field for it.
    snippets = serializers.PrimaryKeyRelatedField(many=True, queryset= Snippet.objects.all())

    class Meta:
        model = User
        fields = ('id','username','snippets')
