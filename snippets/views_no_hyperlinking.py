from rest_framework import mixins, generics , permissions
from snippets.models import Snippet
from snippets.serializers import SnippetSerializer, UserSerializer
from django.contrib.auth.models import User
from .permissions import IsOwnerOrReadOnly
#One of the big wins of using class-based views is that it allows us to easily compose reusable bits of behaviour.
class SnippetList(generics.ListCreateAPIView):
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)  
    # The user isn't sent as part of the serialized representation, but is instead a property of the incoming request.
    # allows us to modify how the instance save is managed, 
    # and handle any information that is implicit in the incoming request or requested URL.
    def perform_create(self, serializer):
        serializer.save(owner = self.request.user)


        
class SnippetDetail(generics.RetrieveUpdateDestroyAPIView):
    """ Retrieve, update or delete a code snippet."""
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)  

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer