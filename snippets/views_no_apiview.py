from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from snippets.models import Snippet
from snippets.serializers import SnippetSerializer

#ote that because we want to be able to POST to this view from clients that won't have a CSRF token we need to mark the view as csrf_exempt
@csrf_exempt
def snippet_list(request):
    """List all code snippets, or create a new snippet"""
    if request.method == 'GET':
        snippets = Snippet.objects.all()
        serializer= SnippetSerializer(snippets,many=True)
        return JsonResponse(serializer.data,safe=False)

    elif request.method == 'POST':
        data  =JSONParser().parse(request)
        serializer = SnippetSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors,status=400)

# REST framework introduces a Request object that extends the regular HttpRequest, and provides more flexible request parsing. 
# The core functionality of the Request object is the request.data attribute, 
# which is similar to request.POST, but more useful for working with Web APIs.
@csrf_exempt
def snippet_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        snippet = Snippet.objects.get(pk=pk)
    except Snippet.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = SnippetSerializer(snippet)
        return JsonResponse(serializer.data)
    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = SnippetSerializer(snippet,data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)
    elif request.method == 'DELETE':
        snippet.delete()
        return HttpResponse(status = 204)
# It's worth noting that there are a couple of edge cases we're not dealing with properly at the moment. 
# If we send malformed json, or if a request is made with a method that the view doesn't handle, 
# then we'll end up with a 500 "server error" response. Still, this'll do for now.