from rest_framework import serializers
from .models import Snippet, LANGUAGE_CHOICES, STYLE_CHOICES
from django.contrib.auth.models import User
# Register your models here.
class SnippetSerializer(serializers.HyperlinkedModelSerializer):
    #The first part of the serializer class defines the fields that get serialized/deserialized
    # It's important to remember that ModelSerializer classes don't do anything particularly magical, 
    # they are simply a shortcut for creating serializer classes:
        # An automatically determined set of fields.
        # Simple default implementations for the create() and update() methods.
        
    # The untyped ReadOnlyField is always read-only, and will be used for serialized representations, 
    # but will not be used for updating model instances when they are deserialized
    owner = serializers.ReadOnlyField(source='owner.username')
    highlight = serializers.HyperlinkedIdentityField(view_name='snippet-highlight', format='html')
    class Meta:
        model = Snippet
        fields = ('url', 'id', 'highlight', 'owner',
                  'title', 'code', 'linenos', 'language', 'style')

# The HyperlinkedModelSerializer has the following differences from ModelSerializer:

    # It does not include the id field by default.
    # It includes a url field, using HyperlinkedIdentityField.
    # Relationships use HyperlinkedRelatedField, instead of PrimaryKeyRelatedField.

# Our snippet and user serializers include 'url' fields that by default will refer to 
# '{model_name}-detail', which in this case will be 'snippet-detail' and 'user-detail'
class UserSerializer(serializers.HyperlinkedModelSerializer):
    # Because 'snippets' is a reverse relationship on the User model, it will not be included by 
    # default when using the ModelSerializer class, so we needed to add an explicit field for it.
    snippets = serializers.HyperlinkedRelatedField(many=True, view_name='snippet-detail', read_only=True)

    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'snippets')
