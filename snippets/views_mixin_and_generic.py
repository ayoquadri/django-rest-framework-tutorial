from rest_framework import mixins, generics
from snippets.models import Snippet
from snippets.serializers import SnippetSerializer

#One of the big wins of using class-based views is that it allows us to easily compose reusable bits of behaviour.
class SnippetList(mixins.ListModelMixin, 
                mixins.CreateModelMixin, 
                generics.GenericAPIView):
    queryset = Snippet.objects.all();
    serializer_class = SnippetSerializer

    """List all code snippets, or create a new snippet"""

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

        
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
        
class SnippetDetail(mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin, 
                    mixins.DestroyModelMixin, 
                    generics.GenericAPIView):
    """ Retrieve, update or delete a code snippet."""
    queryset = Snippet.objects.all();
    serializer_class = SnippetSerializer
    
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

        
    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)